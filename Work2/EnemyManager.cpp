#include"DxLib.h"
#include"Object.h"
#include"Enemy.h"
#include"AEnemy.h"
#include"SEnemy.h"
#include"DEnemy.h"
#include"EnemyBulletManager.h"
#include"EnemyManager.h"
#include"Define.h"

CEnemyManager::CEnemyManager()
{
	for (int num = 0; num < ENEMY_NUM; num++)
	{
		enemy[num] = NULL;
	}
	//敵の画像読み込み
	AEnemyGraphic = LoadGraph("img/Object/AEnemy.png");
	SEnemyGraphic = LoadGraph("img/Object/SEnemy.png");
	DEnemyGraphic = LoadGraph("img/Object/DEnemy.png");
	enemyBulletGraphic = new int[24];
	LoadDivGraph("img/Shot/16Shots.png", 24, 12, 2, E_B_CHIP_SIZE, E_B_CHIP_SIZE, enemyBulletGraphic, TRUE);
}

CEnemyManager::~CEnemyManager()
{
	for (int num = 0; num < ENEMY_NUM; num++)
	{
		delete enemy[num];
	}
	delete[] enemyBulletGraphic;
}

void CEnemyManager::Update()
{
	Spawn();
	for (int num = 0; num < ENEMY_NUM; num++)
	{
		//NULLでない場合
		if (enemy[num] != NULL)
		{
			enemy[num]->Update();
			if (enemy[num]->GetCnt() % E_INTERVAL == E_INTERVAL / 2)
			{
				VECTOR firstpos = *enemy[num]->GetPos();
				firstpos.y = firstpos.y - (E_B_CHIP_SIZE / 2);
				if (num < aEnemy)
				{
					enemyBulletManager->AShot(enemyBulletGraphic[21], enemyBulletGraphic[22], enemyBulletGraphic[23], firstpos);
				}
				else if (num < sEnemy)
				{
					enemyBulletManager->SShot(enemyBulletGraphic[21], enemyBulletGraphic[22], enemyBulletGraphic[23], firstpos);
				}
				else if (num < dEnemy)
				{
					enemyBulletManager->DShot(enemyBulletGraphic[21], enemyBulletGraphic[22], enemyBulletGraphic[23], firstpos);
				}
			}
			//敵が画面外に出た場合
			if (enemy[num]->GetFlag() == false)
			{
				//削除してからNULLを入れる
				delete enemy[num];
				enemy[num] = NULL;
			}
		}
	}
}

void CEnemyManager::Spawn()
{
	//一定確立で敵が出現
	if ((rand() % 30) == 0)
	{
		//敵を生成するX軸を決める
		int yPos = (rand() % (SCREEN_WIDTH));
		VECTOR VPos = VGet(SCREEN_WIDTH, (float)yPos, 0);
		switch (rand() % 3)
		{
		case(0):
			for (int num = 0; num < aEnemy; num++)
			{
				//NULLの場合
				if (enemy[num] == NULL)
				{
					//敵生成
					enemy[num] = new AEnemy(&AEnemyGraphic, VPos);
					//一体生成したら抜けるようにする
					break;
				}
			}
			break;
		case(1):
			for (int num = aEnemy; num < sEnemy; num++)
			{
				//NULLの場合
				if (enemy[num] == NULL)
				{
					//敵生成
					enemy[num] = new SEnemy(&SEnemyGraphic, VPos);
					//一体生成したら抜けるようにする
					break;
				}
			}
			break;
		case(2):
			for (int num = sEnemy; num < dEnemy; num++)
			{
				//NULLの場合
				if (enemy[num] == NULL)
				{
					//敵生成
					enemy[num] = new DEnemy(&DEnemyGraphic, VPos);
					//一体生成したら抜けるようにする
					break;
				}
			}
			break;
		}
	}
}

void CEnemyManager::Render()
{
	for (int num = 0; num < ENEMY_NUM; num++)
	{
		//NULLでない場合
		if (enemy[num] != NULL)
		{
			enemy[num]->Render();
		}
	}
}

void CEnemyManager::HitAction(int num, int power)
{
	enemy[num]->HitAction(power);
}

VECTOR CEnemyManager::GetEnemyPos(int num)
{
	return *enemy[num]->GetPos();
}

bool CEnemyManager::GetInvincible(int num)
{
	return enemy[num]->GetInvincible();
}