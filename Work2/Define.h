#pragma once
/**************************************************************************************************************/
//スクリーンサイズ
#define SCREEN_HEIGHT 760
#define SCREEN_HEIGHT_HALF (SCREEN_HEIGHT/2)
#define SCREEN_WIDTH  1600
#define SCREEN_WIDTH_HALF  (SCREEN_WIDTH/2)
/**************************************************************************************************************/
//プレイヤー
//初期座標
#define P_DEF_X	32
#define P_DEF_Y SCREEN_HEIGHT_HALF
//チップサイズ
#define P_CHIP_SIZE 32
//スピード
#define P_SPEED 10
//ノックバックするスピード
#define NOCKBACK 30
//被弾時のノックバック距離
#define NOCKBACKDISTANCE 40
/**************************************************************************************************************/
//エネミー
//チップサイズ
#define E_CHIP_SIZE 32
//体力
#define E_HP 60
//スピード
#define ENEMY_SPEED -0.01
//弾の速度
#define E_B_SPEED_X 10
//アニメーション速度
#define E_ANIM_SPEED 100
//弾の発射間隔
#define E_INTERVAL 80
//弾のチップサイズ
#define E_B_CHIP_SIZE 16
/**************************************************************************************************************/
//Shot
//移動速度
#define Z_SPEED_X 10
#define Z_SPEED_Y 0
#define X_SPEED_X 3
#define X_SPEED_Y 0
#define C_SPEED_X 10
#define C_SPEED_Y 15
#define V_SPEED_X 5
#define V_SPEED_Y 5
//チップサイズ
#define Z_CHIP_SIZE 16
#define X_CHIP_SIZE 64
#define C_CHIP_SIZE 16
#define V_CHIP_SIZE 20
//アニメスピード
#define Z_ANIM_SPEED 100
#define X_ANIM_SPEED 100
#define C_ANIM_SPEED 100
#define V_ANIM_SPEED 100
//発射間隔
#define Z_INTERVAL 50
#define X_INTERVAL 150
#define C_INTERVAL 30
#define V_INTERVAL 10
//威力
#define Z_POWER 1
#define X_POWER 10
#define C_POWER 3
#define V_POWER 2
/**************************************************************************************************************/