#include"DxLib.h"
#include"Manager.h"
#include"Define.h"
#include"BulletManager.h"

#include"Player.h"


CPlayer::CPlayer(CManager *pManager)
{
	//CManagerのアドレス格納
	manager = pManager;
	//画像データ格納
	graphic = new int;
	*graphic = LoadGraph("img/Object/Player.png");

	//位置を初期化
	pos = VGet(P_DEF_X, P_DEF_Y, 0);

	//球画像の枚数分のメモリーを確保する
	zBulletGraphic = new int[24];
	LoadDivGraph("img/Shot/16Shots.png", 24, 12, 2, Z_CHIP_SIZE, Z_CHIP_SIZE, zBulletGraphic, TRUE);
	xBulletGraphic = new int[24];
	LoadDivGraph("img/Shot/64Shots.png", 24, 12, 2, X_CHIP_SIZE, X_CHIP_SIZE, xBulletGraphic, TRUE);
	cBulletGraphic = new int[12];
	LoadDivGraph("img/Shot/24Shots.png", 12, 12, 1, C_CHIP_SIZE, C_CHIP_SIZE, cBulletGraphic, TRUE);
	vBulletGraphic = new int[4];
	LoadDivGraph("img/Shot/24Shots.png", 4, 4, 1, V_CHIP_SIZE, V_CHIP_SIZE, vBulletGraphic, TRUE);

	zMultiFlag = false;

}

CPlayer::~CPlayer()
{
	delete graphic;
	delete[] zBulletGraphic;
	delete[] xBulletGraphic;
	delete[] cBulletGraphic;
	delete[] vBulletGraphic;
}

void CPlayer::Update()
{
	Move();
	ZShot();
	XShot();
	CShot();
	VShot();
	if (invincible)
	{
		NockBack();
		if (nockBackDis > pos.x||pos.x<=0)
		{
			invincible = false;
		}
	}
}

void CPlayer::NockBack()
{
	pos.x -= NOCKBACK;
}

void CPlayer::HitAction() 
{
	hp--; 
	invincible = true;
	nockBackDis = pos.x - NOCKBACKDISTANCE;
}

void CPlayer::Move()
{
	//移動する向き
	VECTOR vPos = VGet(0, 0, 0);

	//移動していないときは正規化する必要がないのでこれで移動しているか確認する
	bool moveFlag = false;
	if (manager->GetKey()[KEY_INPUT_LEFT] > 0)
	{
		vPos.x -= 1.0f;
		moveFlag = true;
	}
	if (manager->GetKey()[KEY_INPUT_RIGHT] > 0)
	{
		vPos.x += 1.0f;
		moveFlag = true;
	}
	if (manager->GetKey()[KEY_INPUT_UP] > 0)
	{
		vPos.y -= 1.0f;
		moveFlag = true;
	}
	if (manager->GetKey()[KEY_INPUT_DOWN] > 0)
	{
		vPos.y += 1.0f;
		moveFlag = true;
	}
	//フラグが立っていてかつvPosが0でないときに座標を計算する
	if (moveFlag&& VSquareSize(vPos) != 0)
	{
		//vPosを正規化
		vPos = VNorm(vPos);
		//vPosをP_SPEED倍する
		vPos = VScale(vPos, P_SPEED);
		//現在の座標に移動量を加算する
		pos = VAdd(pos, vPos);
	}
	MoveRestriction();
}

void CPlayer::MoveRestriction()
{
	if (pos.x < 0)
	{
		pos.x = 0;
	}
	if (pos.x > SCREEN_WIDTH - P_CHIP_SIZE)
	{
		pos.x = SCREEN_WIDTH - P_CHIP_SIZE;
	}
	if (pos.y < 0)
	{
		pos.y = 0;
	}
	if (pos.y > SCREEN_HEIGHT - P_CHIP_SIZE)
	{
		pos.y = SCREEN_HEIGHT - P_CHIP_SIZE;
	}
}

/**************************************************************************************************************/
//Shot全部の発射プログラム
void CPlayer::ZShot()
{
	//弾の発射間隔
	static int interval = 0;
	//弾の連射間隔
	static int zMultiShotInterval=0;
	//連射数
	static int varstCnt=0;

	if (manager->GetKey()[KEY_INPUT_Z] > 0)
	{
		if (interval >= Z_INTERVAL)
		{
			zMultiFlag = true;
		}
	}
	if (interval <= Z_INTERVAL)
	{
		interval++;
	}
	if (zMultiFlag)
	{
		zMultiShotInterval++;
		if (zMultiShotInterval >= 5)
		{
			zMultiShotInterval = 0;
			varstCnt++;
			//プレイヤーの中心座標Firstpos
			VECTOR firstpos = VGet(pos.x, pos.y + (P_CHIP_SIZE / 2) - (Z_CHIP_SIZE / 2), 0);
			//上に弾を飛ばす
			bulletManager->ZShot(zBulletGraphic[0], zBulletGraphic[1], zBulletGraphic[2], firstpos);
		}
		if (varstCnt >= 3)
		{
			interval = 0;
			zMultiFlag = false;
			zMultiShotInterval = 0;
			varstCnt = 0;
		}
	}
}

void CPlayer::XShot()
{
	//弾の発射を抑制するトリガー
	static int interval = 0;
	//SPACEを押したとき、intervalとINTERVALが等しかったら発射
	if (manager->GetKey()[KEY_INPUT_X] > 0)
	{
		if (interval >= X_INTERVAL)
		{
			interval = 0;
			//プレイヤーの中心座標Firstpos
			VECTOR firstpos = VGet(pos.x, pos.y + (P_CHIP_SIZE / 2) - (Z_CHIP_SIZE / 2), 0);
			//上に弾を飛ばす
			bulletManager->XShot(xBulletGraphic[0], xBulletGraphic[1], xBulletGraphic[2], firstpos);
		}
	}
	//intervalがINTERVALより小さかったらintervalが増え続ける
	if (interval <= X_INTERVAL)
	{
		interval++;
	}
}

void CPlayer::CShot()
{
	//弾の発射を抑制するトリガー
	static int interval = 0;
	//SPACEを押したとき、intervalとINTERVALが等しかったら発射
	if (manager->GetKey()[KEY_INPUT_C] > 0)
	{
		if (interval >= C_INTERVAL)
		{
			interval = 0;
			//プレイヤーの中心座標Firstpos
			VECTOR firstpos = VGet(pos.x, pos.y + (P_CHIP_SIZE / 2) - (C_CHIP_SIZE / 2), 0);
			//上に弾を飛ばす
			bulletManager->CShot(zBulletGraphic[0], zBulletGraphic[1], zBulletGraphic[2], firstpos);
		}
	}
	//intervalがINTERVALより小さかったらintervalが増え続ける
	if (interval <= C_INTERVAL)
	{
		interval++;
	}
}

void CPlayer::VShot()
{
	//弾の発射を抑制するトリガー
	static int interval = 0;
	//SPACEを押したとき、intervalとINTERVALが等しかったら発射
	if (manager->GetKey()[KEY_INPUT_V] > 0)
	{
		if (interval >= V_INTERVAL)
		{
			interval = 0;
			//プレイヤーの中心座標Firstpos
			VECTOR firstpos = VGet(pos.x, pos.y + (P_CHIP_SIZE / 2) - (V_CHIP_SIZE / 2), 0);
			//上に弾を飛ばす
			bulletManager->VShot(vBulletGraphic[0], vBulletGraphic[1], vBulletGraphic[2], firstpos);
		}
	}
	//intervalがINTERVALより小さかったらintervalが増え続ける
	if (interval <= V_INTERVAL)
	{
		interval++;
	}
}
/**************************************************************************************************************/

void CPlayer::Render()
{
	DrawGraph((int)pos.x, (int)pos.y, *graphic, TRUE);
}

