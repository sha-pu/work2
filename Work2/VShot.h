#pragma once

#include "Bullet.h"

//自分の少し先を中心に回転する弾
class VBullet :public Bullet
{
public:
	VBullet(int f, int s, int t, VECTOR &position);
	~VBullet();

	void Update();
	void Render();
private:
	//アニメーション3枚の格納先
	int graphic_f;
	int graphic_s;
	int graphic_t;
	//発射された座標
	VECTOR firstpos;
	//三角関数に入れる角度
	float rad ;
};