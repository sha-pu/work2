#pragma once

class Bullet;

#define BULLET_NUM 100
#define BULLET_Z BULLET_NUM/4
#define BULLET_X BULLET_NUM/4
#define BULLET_C BULLET_NUM/4
#define BULLET_V BULLET_NUM/4

enum
{
	zSHOT = BULLET_Z,
	xSHOT = BULLET_X + BULLET_X,
	cSHOT = BULLET_Z + BULLET_X + BULLET_C,
	vSHOT = BULLET_Z + BULLET_Z + BULLET_C + BULLET_V
};

class CBulletManager
{
	Bullet *bullet[BULLET_NUM];
public:
	CBulletManager();
	~CBulletManager();
	//現座標を返す
	VECTOR GetBulletPos(int num);
	//発射処理
	void ZShot(int f, int s, int t, VECTOR &pos);
	void XShot(int f, int s, int t, VECTOR &pos);
	void CShot(int f, int s, int t, VECTOR &pos);
	void VShot(int f, int s, int t, VECTOR &pos);
	//bulletの状態をCBulletManager型で返す
	CBulletManager *GetBullet(int num) { return (CBulletManager*)bullet[num]; }
	//継承元参照
	void Update();
	void Render();
	void HitAction(int num) ;
	//void SetBulletManager(CBulletManager *bullet) { enemyBulletManager = bullet; }
};