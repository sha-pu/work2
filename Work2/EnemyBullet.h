#pragma once

#include"Object.h"

//弾の管理
class CEnemyBullet :public CObject
{
public:
	/*CEnemyBullet(int f, int s, int t, VECTOR &position);
	~CEnemyBullet();*/
	//進む方向ベクトル
	VECTOR vPos;
	//弾の生存フラグ(true = 生きている)
	bool flag;

	virtual void Render()=0;
	virtual void Update()=0;

	bool GetFlag() { return flag; };
	void HitAction() { flag = false; }
};
