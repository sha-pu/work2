#pragma once

class CPlayer;
class CBulletManager;
class CEnemyManager;
class CEnemyBulletManager;

class CGame :public CScene
{
	CPlayer *player;
	CBulletManager *bulletManager;
	CEnemyManager *enemyManager;
	CEnemyBulletManager *enemyBulletManager;
public:
	CGame(CManager *pManager);
	~CGame();
	void Update();
	void Render();
};