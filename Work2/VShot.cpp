#include "DxLib.h"
#include "VShot.h"
#include "Define.h"

VBullet::VBullet(int f, int s, int t, VECTOR &position)
{
	firstpos = position;
	vPos = VGet(0, 0, 0);
	graphic_f = f;
	graphic_s = s;
	graphic_t = t;
	pos = position;
	flag = true;
	rad = 0;
}

VBullet::~VBullet()
{

}

#include<math.h>

void VBullet::Update()
{
	rad += 0.01f;
	vPos = VGet((float)sin(rad * 5)*V_SPEED_X, (float)sin((rad - 0.32) * 5)*V_SPEED_Y, 0);
	if (rad > 1.25)
	{
		flag = false;
	}
	pos = VAdd(pos, vPos);
	if (Delete_Screen(pos.x, pos.y))
	{
		flag = false;
	}
}

void VBullet::Render()
{
	int anim_cnt = GetNowCount();
	anim_cnt %= (4 * V_ANIM_SPEED);
	int img = 0;
	switch (anim_cnt / V_ANIM_SPEED)
	{
	case(0):img = graphic_f; break;
	case(1):img = graphic_s; break;
	case(2):img = graphic_t; break;
	case(3):img = graphic_s; break;
	}
	DrawGraph((int)pos.x, (int)pos.y, img, TRUE);

}