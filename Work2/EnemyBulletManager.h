#pragma once

class CEnemyBullet;

#define ENEMY_BULLET_NUM 100


class CEnemyBulletManager
{
	CEnemyBullet *enemyBullet[ENEMY_BULLET_NUM];
public:
	CEnemyBulletManager();
	~CEnemyBulletManager();
	//現座標を返す
	VECTOR GetBulletPos(int num);
	//発射処理
	void AShot(int f, int s, int t, VECTOR &pos);
	void SShot(int f, int s, int t, VECTOR &pos);
	void DShot(int f, int s, int t, VECTOR &pos);
	//bulletの状態をCEnemyBulletManager型で返す
	CEnemyBulletManager *GetBullet(int num) { return (CEnemyBulletManager*)enemyBullet[num]; }
	//継承元参照
	void Update();
	void Render();
	void HitAction(int num);
};