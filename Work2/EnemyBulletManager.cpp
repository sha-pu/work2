#include"Dxlib.h"
#include"Bullet.h"
#include"EnemyBulletManager.h"
#include"AEnemyShot.h"
#include"SEnemyShot.h"
#include"DEnemyShot.h"
#include"EnemyBullet.h"

CEnemyBulletManager::CEnemyBulletManager()
{
	//弾全ての初期化
	for (int num = 0; num < ENEMY_BULLET_NUM; num++)
	{
		enemyBullet[num] = NULL;
	}
}

CEnemyBulletManager::~CEnemyBulletManager()
{
	//弾全ての削除
	for (int num = 0; num < ENEMY_BULLET_NUM; num++)
	{
		delete enemyBullet[num];
	}
}

void CEnemyBulletManager::AShot(int f, int s, int t, VECTOR &pos)
{
	for (int num = 0; num < ENEMY_BULLET_NUM; num++)
	{
		if (enemyBullet[num] == NULL)
		{
			//画像、座標、速度をEnemyBulletに渡す
			enemyBullet[num] = new AEnemyBullet(f, s, t, pos);
			break;
		}
	}
}

void CEnemyBulletManager::SShot(int f, int s, int t, VECTOR &pos)
{
	for (int num = 0; num < ENEMY_BULLET_NUM; num++)
	{
		if (enemyBullet[num] == NULL)
		{
			//画像、座標、速度をEnemyBulletに渡す
			enemyBullet[num] = new SEnemyBullet(f, s, t, pos);
			break;
		}
	}
}

void CEnemyBulletManager::DShot(int f, int s, int t, VECTOR &pos)
{
	for (int num = 0; num < ENEMY_BULLET_NUM; num++)
	{
		if (enemyBullet[num] == NULL)
		{
			//画像、座標、速度をEnemyBulletに渡す
			enemyBullet[num] = new DEnemyBullet(f, s, t, pos);
			break;
		}
	}
}

void CEnemyBulletManager::Update()
{
	for (int num = 0; num < ENEMY_BULLET_NUM; num++)
	{
		//NULLでない場合
		if (enemyBullet[num] != NULL)
		{
			enemyBullet[num]->Update();
			//弾が画面外に出た場合
			if (enemyBullet[num]->GetFlag() == false)
			{
				//削除してからNULLを入れる
				delete enemyBullet[num];
				enemyBullet[num] = NULL;
			}
		}
	}
}

void CEnemyBulletManager::Render()
{
	for (int num = 0; num < ENEMY_BULLET_NUM; num++)
	{
		//NULLでない場合
		if (enemyBullet[num] != NULL)
		{
			enemyBullet[num]->Render();
		}
	}
}

void CEnemyBulletManager::HitAction(int num)
{
	enemyBullet[num]->HitAction();
}


VECTOR CEnemyBulletManager::GetBulletPos(int num)
{
	return *enemyBullet[num]->GetPos();
}