#include "DxLib.h"
#include "Manager.h"
#include "Game.h"
#include "Object.h"
#include "Player.h"
#include"BulletManager.h"
#include"EnemyManager.h"
#include"EnemyBulletManager.h"
#include "Define.h"

CGame::CGame(CManager *pManager) : CScene(pManager)
{
	//プレイヤーを動的確保
	player = new CPlayer(pManager);
	//弾の全体管理しているクラスを動的確保
	bulletManager = new CBulletManager();
	//CBulletManagerクラスのアドレス取得
	player->SetBulletManager(bulletManager);
	//CEnemyManagerを動的確保
	enemyManager = new CEnemyManager();
	//CEnemyBulletManagerを動的確保
	enemyBulletManager = new CEnemyBulletManager();
	//CEnemyBulletManagerクラスのアドレスをEnemyManagerで取得
	enemyManager->SetBulletManager(enemyBulletManager);
}

CGame::~CGame()
{
	delete player;
	delete bulletManager;
	delete enemyManager;
	delete enemyBulletManager;
}

void CGame::Update()
{
	player->Update();
	bulletManager->Update();
	enemyManager->Update();
	enemyBulletManager->Update();

	//現在画面内にいる敵と自分の球の当たり判定
	for (int i = 0; i < BULLET_NUM; i++)
	{
		if (bulletManager->GetBullet(i) != NULL)
		{
			for (int j = 0; j < ENEMY_NUM; j++)
			{
				if (enemyManager->GetEnemy(j) != NULL)
				{
					int chipsize = 0;
					int power = 0;
					//SHOTによって威力、サイズが違うためif文で代入する値を変える
					if (i < zSHOT)
					{
						chipsize = Z_CHIP_SIZE;
						power = Z_POWER;
					}
					else if (i < xSHOT)
					{
						chipsize = X_CHIP_SIZE;
						power = X_POWER;
					}
					else if (i < cSHOT)
					{
						chipsize = C_CHIP_SIZE;
						power = C_POWER;
					}
					else if (i < vSHOT)
					{
						chipsize = V_CHIP_SIZE;
						power = V_POWER;
					}
					//当たり判定関数に弾、敵を投げる
					if (CheckHit_Object(bulletManager->GetBulletPos(i), chipsize / 2, enemyManager->GetEnemyPos(j), E_CHIP_SIZE / 2))
					{
						//敵が被弾直後の無敵状態でなかったら被弾処理を行う
						if (enemyManager->GetInvincible(j)==false)
						{
							bulletManager->HitAction(i);
							enemyManager->HitAction(j, power);
						}
					}
				}
			}
		}
	}

	//現在画面内にいる敵と自分の球の当たり判定
	for (int i = 0; i < ENEMY_BULLET_NUM; i++)
	{
		if (enemyBulletManager->GetBullet(i) != NULL)
		{
			for (int j = 0; j < ENEMY_BULLET_NUM; j++)
			{
				if (enemyManager->GetEnemy(j) != NULL)
				{
					//当たり判定関数に敵の弾、プレイヤーを投げる
					if (CheckHit_Object(enemyBulletManager->GetBulletPos(i), E_B_CHIP_SIZE / 2, *player->GetPos(), P_CHIP_SIZE / 2))
					{
						//プレイヤーが被弾直後の無敵状態でなかったら被弾処理を行う
						if (player->GetInvincible() == false)
						{
							enemyBulletManager->HitAction(i);
							player->HitAction();
						}
						//DrawFormatString(100, 200, GetColor(255, 255, 255), "あたり！！！！！！！！！！！！！！！");
					}
				}
			}
		}
	}
}

void CGame::Render()
{
	DrawFormatString(0, 0, GetColor(255, 255, 255), "ゲーム");
	bulletManager->Render();
	player->Render();
	enemyManager->Render();
	enemyBulletManager->Render();
}
