#include"Dxlib.h"
#include"Bullet.h"
#include"BulletManager.h"
#include"ZShot.h"
#include"XShot.h"
#include"CShot.h"
#include"VShot.h"

CBulletManager::CBulletManager()
{
	//弾全ての初期化
	for (int num = 0; num < BULLET_NUM; num++)
	{
		bullet[num] = NULL;
	}
}

CBulletManager::~CBulletManager()
{
	//弾全ての削除
	for (int num = 0; num < BULLET_NUM; num++)
	{
		delete bullet[num];
	}
}

void CBulletManager::ZShot(int f, int s, int t, VECTOR &pos )
{
	for (int num = 0; num < BULLET_Z; num++)
	{
		if (bullet[num] == NULL)
		{
			//画像、座標、速度をBulletに渡す
			bullet[num] = new ZBullet (f, s, t, pos);
			break;
		}
	}
}

void CBulletManager::XShot(int f, int s, int t, VECTOR &pos)
{
	for (int num = BULLET_Z; num < BULLET_Z+BULLET_X; num++)
	{
		if (bullet[num] == NULL)
		{
			//画像、座標、速度をCBulletに渡す
			bullet[num] = new XBullet(f, s, t, pos);
			break;
		}
	}
}

void CBulletManager::CShot(int f, int s, int t, VECTOR &pos)
{
	for (int num = BULLET_Z+BULLET_X; num < BULLET_Z + BULLET_X+BULLET_C-1; num++)
	{
		if (bullet[num] == NULL)
		{
			//画像、座標、速度をCBulletに渡す
			bullet[num] = new CBullet(f, s, t, pos);
			bullet[num+1] = new CBullet_U(f, s, t, pos);
			break;
		}
	}
}

void CBulletManager::VShot(int f, int s, int t, VECTOR &pos)
{
	for (int num = BULLET_Z + BULLET_X + BULLET_C; num < BULLET_Z + BULLET_X + BULLET_C+BULLET_V; num++)
	{
		if (bullet[num] == NULL)
		{
			//画像、座標、速度をCBulletに渡す
			bullet[num] = new VBullet(f, s, t, pos);
			break;
		}
	}
}

void CBulletManager::Update()
{
	for (int num = 0; num < BULLET_NUM; num++)
	{
		//NULLでない場合
		if (bullet[num] != NULL)
		{
			bullet[num]->Update();
			//弾が画面外に出た場合
			if (bullet[num]->GetFlag() == false)
			{
				//削除してからNULLを入れる
				delete bullet[num];
				bullet[num] = NULL;
			}
		}
	}
}

void CBulletManager::Render()
{
	for (int num = 0; num < BULLET_NUM; num++)
	{
		//NULLでない場合
		if (bullet[num] != NULL)
		{
			bullet[num]->Render();
		}
	}
}

void CBulletManager::HitAction(int num)
{
	bullet[num]->HitAction();
}

VECTOR CBulletManager::GetBulletPos(int num)
{
	return *bullet[num]->GetPos();
}