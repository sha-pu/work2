#pragma once

#include"Object.h"

//弾の管理
class Bullet :public CObject
{
public:
	/*CBullet(int f, int s, int t, VECTOR &position);
	~CBullet();*/
	//進む方向ベクトル
	VECTOR vPos;
	//弾の生存フラグ(true = 生きている)
	bool flag;
	//描画
	virtual void Render()=0;
	//更新
	virtual void Update()=0;
	//現在の生存フラグを返す
	bool GetFlag() { return flag; };
	//被弾時に消える処理
	void HitAction() { flag = false; }
};

//画面外にいたらフラグを倒す
int Delete_Screen(float x,float y);
