#pragma once

class CEnemy;
class CEnemyBulletManager;

//敵の数
#define ENEMY_NUM 90
#define AENEMY ENEMY_NUM/3
#define SENEMY ENEMY_NUM/3
#define DENEMY ENEMY_NUM/3

enum
{
	aEnemy = AENEMY,
	sEnemy = AENEMY + SENEMY,
	dEnemy = AENEMY + SENEMY + DENEMY
};

class CEnemyManager
{
	CEnemy *enemy[ENEMY_NUM];
	CEnemyBulletManager *enemyBulletManager;
public:
	CEnemyManager();
	~CEnemyManager();
	//現座標を返す
	VECTOR GetEnemyPos(int num);
	//弾の画像
	int *enemyBulletGraphic;
	//敵の画像
	int AEnemyGraphic;
	int SEnemyGraphic;
	int DEnemyGraphic;
	//自分が今被弾中で無敵状態かを返す
	bool GetInvincible(int num);

	//Enemyの状態をCEnemytManager型で返す
	CEnemyManager *GetEnemy(int num) { return (CEnemyManager*)enemy[num]; }
	//EnemyBulletManagerのアドレスを取得する
	void SetBulletManager(CEnemyBulletManager *bullet) { enemyBulletManager = bullet; }
	//継承元参照
	void Update();
	void Render();
	void HitAction(int num, int power);
private:
	//敵の生成
	void Spawn();
};