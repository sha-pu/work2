#pragma once

#include "Object.h"
#include "Define.h"

class CEnemy : public CObject
{
public:
	/*CEnemy(int *tex, VECTOR &position);
	~CEnemy();*/
	//敵のHP
	int hp;
	//進む方向ベクトル
	VECTOR vPos;
	//敵の生存フラグ(true = 生きている)
	bool flag;
	//弾の発射感覚カウント
	int Cnt;

	//発射感覚カウントを変えす
	int GetCnt() { return Cnt; }
	//自分が今被弾中で無敵状態かを返す
	bool GetInvincible() { return invincible; }
	//敵の生存フラグを取得
	bool GetFlag() { return flag; };

	//無敵状態、true=Hit直後
	bool invincible;
	//被弾時に後退する関数
	void NockBack() { pos.x += NOCKBACK; }
	//後退する距離
	float nockBackDis;

	virtual void Render() = 0;
	virtual void Update() = 0;
	void HitAction(int power) 
	{
		hp -= power;
		invincible = true;
		nockBackDis = pos.x + NOCKBACKDISTANCE;
	};
};

//撃破条件
int EnemyDeleteCondition(float x, int hp);