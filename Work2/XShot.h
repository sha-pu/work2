#pragma once

#include "Bullet.h"

//直線状に高速で発射される弾
class XBullet :public Bullet
{
public:
	XBullet(int f, int s, int t, VECTOR &position);
	~XBullet();

	void Update();
	void Render();
private:
	float speed;
	//アニメーション3枚の格納先
	int graphic_f;
	int graphic_s;
	int graphic_t;
};