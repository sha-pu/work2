#pragma once

#include"DxLib.h"

class CObject
{
public:
	CObject() {};
	~CObject() {};

	//座標
	VECTOR pos;
	//画像のアドレス
	int *graphic;
	//更新
	virtual void Update()=0;
	//描画
	virtual void Render()=0;

	////posのアドレスを渡す関数
	VECTOR *GetPos() { return &pos; }

	//当たった時の処理(全て処理が違うので仮想関数にする)
	virtual void HitAction() {};
};
//２つのオブジェクトの当たり判定
int CheckHit_Object(VECTOR obj1, int obj1range, VECTOR obj2, int obj2range);