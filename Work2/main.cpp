#include "DxLib.h"
#include "Manager.h"
#include "Game.h"
#include "Define.h"

#define KEY_NUM 256

//http://haina.hatenablog.com/entry/2016/09/25/004108

int GpUpdateKey(char *key);
bool Process(char *key);
// プログラムは WinMain から始まります
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	//ウインドウモードに変更
	ChangeWindowMode(TRUE);

	//DXライブラリを初期化する。
	if (DxLib_Init() == -1) return -1;

	//キー取得
	char key[KEY_NUM];

	//描画先画面を裏にする
	SetGraphMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32);

	SetDrawScreen(DX_SCREEN_BACK);

	//管理システムを動的確保
	CManager *manager;
	manager = new CManager(key);
	//シーンを動的確保
	manager->scene = new CGame(manager);

	while (Process(key))
	{
		//ゲームループ
		manager->Render();
		manager->Update();
	}
	//後始末
	delete manager;
	//DXライブラリを終了する。
	DxLib_End();

	return 0;
}

//キーの入力状態を更新する
int GpUpdateKey(char *key)
{
	//現在のキーの入力状態を格納する
	char tmpKey[KEY_NUM];
	//全てのキーの入力状態を得る
	GetHitKeyStateAll(tmpKey);
	for (int i = 0; i < KEY_NUM; i++) {
		if (tmpKey[i] != 0)
		{//i番のキーコードに対応するキーが押されていたら加算
			if (key[i] == 120)
			{
				key[i] = 0;
			}
			key[i]++;
		}
		else
		{//押されていなければ0にする
			key[i] = 0;
		}
	}
	return 0;
}

//画面の更新
bool Process(char *key)
{
	if (ProcessMessage() != 0) return false;
	//画面の裏ページの内容を表ページに反映する
	if (ScreenFlip() != 0) return false;
	//画面を初期化
	if (ClearDrawScreen() != 0) return false;
	//キー取得
	if (GpUpdateKey(key) != 0) return false;
	//エスケープで終了
	if (key[KEY_INPUT_ESCAPE] >= 1) return false;
	return true;
}