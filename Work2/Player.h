#pragma once

#include"Object.h"

class CManager;
class CBulletManager;

class CPlayer :public CObject
{
	CManager *manager;
	//全ての球を管理するポインタ
	CBulletManager *bulletManager;
public:
	//コンストラクタ（インスタンス生成時に最初に呼ばれる関数）
	CPlayer(CManager *);
	//デストラクタ（インスタンス削除時に呼ばれる関数）
	~CPlayer();
	int hp;
	//弾の座標
	int *zBulletGraphic;
	int *xBulletGraphic;
	int *cBulletGraphic;
	int *vBulletGraphic;
	//自分が今被弾中で無敵状態かを返す
	bool GetInvincible() { return invincible; }
	//bulletManagerのアドレスを取得
	void SetBulletManager(CBulletManager *bullet) { bulletManager = bullet; }
	//継承先
	void HitAction();
	void Render();
	void Update();
private:
	//無敵状態、true=Hit直後
	bool invincible;
	//移動ルーチン
	void Move();
	//→画面端から出れないように移動を制限する
	void MoveRestriction();
	//球の発射
	void ZShot();
	void XShot();
	void CShot();
	void VShot();

	//弾zを発射する際に立つフラグ。連射が終わったら倒れる
	bool zMultiFlag;
	//被弾時に後退する関数
	void NockBack();
	//後退する距離
	float nockBackDis;
};