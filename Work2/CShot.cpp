#include "DxLib.h"
#include "CShot.h"
#include "Define.h"

CBullet::CBullet(int f, int s, int t, VECTOR &position)
{
	firstpos = position;
	vPos = VGet(0, 0, 0);
	graphic_f = f;
	graphic_s = s;
	graphic_t = t;
	pos = position;
	flag = true;
}

CBullet::~CBullet()
{

}

#define C_WIDE 64

void CBullet::Update()
{
	float speed=0;
	if (pos.y >= firstpos.y - C_WIDE)
	{
		speed -= C_SPEED_Y;
	}
	vPos = VGet(C_SPEED_X, speed, 0);
	pos = VAdd(pos, vPos);
	if (Delete_Screen(pos.x, pos.y))
	{
		flag = false;
	}
}

void CBullet::Render()
{
	int anim_cnt = GetNowCount();
	anim_cnt %= (4 * C_ANIM_SPEED);
	int img = 0;
	switch (anim_cnt / C_ANIM_SPEED)
	{
	case(0):img = graphic_f; break;
	case(1):img = graphic_s; break;
	case(2):img = graphic_t; break;
	case(3):img = graphic_s; break;
	}
	DrawGraph((int)pos.x, (int)pos.y, img, TRUE);
}
/***************************************************************************************/
CBullet_U::CBullet_U(int f, int s, int t, VECTOR &position)
{
	firstpos = position;
	vPos = VGet(0, 0, 0);
	graphic_f = f;
	graphic_s = s;
	graphic_t = t;
	pos = position;
	flag = true;
}

CBullet_U::~CBullet_U()
{

}
void CBullet_U::Update()
{
	if (pos.y <= firstpos.y + C_WIDE)
	{
		vPos.y += C_SPEED_Y;
	}
	else
	{
		vPos.y = 0;
	}
	vPos.x = C_SPEED_X;
	pos = VAdd(pos, vPos);
	if (Delete_Screen(pos.x, pos.y))
	{
		flag = false;
	}
}

void CBullet_U::Render()
{
	int anim_cnt = GetNowCount();
	anim_cnt %= (4 * C_ANIM_SPEED);
	int img = 0;
	switch (anim_cnt / C_ANIM_SPEED)
	{
	case(0):img = graphic_f; break;
	case(1):img = graphic_s; break;
	case(2):img = graphic_t; break;
	case(3):img = graphic_s; break;
	}
	DrawGraph((int)pos.x, (int)pos.y, img, TRUE);
}