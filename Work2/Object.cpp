#include"DxLib.h"
#include"Object.h"

int CheckHit_Object(VECTOR obj1, int obj1range, VECTOR obj2, int obj2range)
{
	float distance= VSquareSize(VSub(obj1, obj2));
	int range = (obj1range + obj2range)*(obj1range + obj2range);

	if (distance < range)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}