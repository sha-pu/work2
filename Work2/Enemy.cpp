#include"Enemy.h"

int EnemyDeleteCondition(float x,int hp)
{
	//画面の縦の長さ+100の数値まで敵が来たら消すようにする
	if (x < 0 - 100)
	{
		return true;
	}
	//体力が0になったら消す
	if (hp <= 0)
	{
		return true;
	}
	return false;
}
