#include "DxLib.h"
#include "XShot.h"
#include "Define.h"

XBullet::XBullet(int f, int s, int t, VECTOR &position)
{
	vPos = VGet(0, 0, 0);
	graphic_f = f;
	graphic_s = s;
	graphic_t = t;
	pos = position;
	flag = true;
	speed = 0;
}

XBullet::~XBullet()
{

}

void XBullet::Update()
{
	speed += X_SPEED_X;
	vPos = VGet(speed, X_SPEED_Y, 0);
	pos = VAdd(pos, vPos);
	if (Delete_Screen(pos.x, pos.y))
	{
		flag = false;
	}
}

void XBullet::Render()
{
	int anim_cnt = GetNowCount();
	anim_cnt %= (4 * X_ANIM_SPEED);
	int img = 0;
	switch (anim_cnt / X_ANIM_SPEED)
	{
	case(0):img = graphic_f; break;
	case(1):img = graphic_s; break;
	case(2):img = graphic_t; break;
	case(3):img = graphic_s; break;
	}
	DrawGraph((int)pos.x, (int)pos.y, img, TRUE);

}