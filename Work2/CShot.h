#pragma once

#include "Bullet.h"

//自分の上下に発射される弾の上
class CBullet :public Bullet
{
public:
	CBullet(int f, int s, int t, VECTOR &position);
	~CBullet();
	
	void Update();
	void Render();

private:
	VECTOR firstpos;
	//アニメーション3枚の格納先
	int graphic_f;
	int graphic_s;
	int graphic_t;
};

//自分の上下に発射される弾の下
class CBullet_U :public Bullet
{
public:
	CBullet_U(int f, int s, int t, VECTOR &position);
	~CBullet_U();

	void Update();

	void Render();

private:
	VECTOR firstpos;
	//アニメーション3枚の格納先
	int graphic_f;
	int graphic_s;
	int graphic_t;
};